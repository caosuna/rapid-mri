import tensorflow as tf
import numpy as np


class NN_Base(tf.Module):
    def __init__(self, name=None, *args, device="GPU", **kwargs):
        super().__init__(name=name)
        self.device = tf.device(device)
        for key, val in kwargs.items():
            setattr(self, key, val)

    def __call__(self, input):
        return self.forward(input)

    def decorate_forward(func):
        """
        Decorates the forward function to require a shape to its input.

        """

        def wrapper(self, input):
            reshape = len(self.input_shape) == len(input.shape)
            if reshape:
                input.resize(1, *input.shape)
            assert self.input_shape == input.shape[1:]

            output = func(self, input)

            if reshape:
                output.resize(*output.shape[1:])
            return output

        return wrapper

    @decorate_forward
    def forward(self, input):
        """
        Identity. To be over-written

        """
        return input

    def train(self, *args, **kwargs):
        """
        Trains the model given a data generator, loss, optimizer, and cutoff callable.
        The generator is required while other parameters have basic default behavior.
        Call syntax:
            model.train(gen=gen,loss=loss,optim=optim,cut=cut,epochs = 1e3)

        Can take in the following *args:
            None

        Can take in the following **kwargs:
            gen:callable = data generator for batch of train and test data
                genargs = positional arguments to the generator callable
                genkwargs = keyword arguments to the generator callable
                gen_out = output of the generator given below call method:
                    gen_out = gen(self,"train"/"validation"/"test",*genargs,**genkwargs)
                    - This should be equivalent to:
                        gen_out = {"in":traininputs,"out":trainoutputs} # for .train()
                        # where traininputs and trainoutputs are of proper dimensionality to model IO

            loss:callable = loss function to be optimized
                lossargs = positional arguments to the loss
                losskwargs = keyword arguments to the loss
                loss_out = output of the loss given below call method:
                    loss_out = loss(self,gen_out,*lossargs,**losskwargs)
                    - This should be equivalent to:
                        loss_out = loss_val # scalar output

            optim:callable = optimizer to be applied to loss function
                optimargs = positional arguments to the optimizer
                optimkwargs = keyword arguments to the optimizer
                optim_out = optimizer which is capable of modifying model parameters
                    - optim_out = optim(self,loss_out,*optimizerargs,**optimizerkwargs)
                    - This should be equivalent to:
                        optim_out = complex variable with information to be utilized in cutoff function

            cut:callable = cutoff function to truncate optimization
                cutargs = positional arguments to the cutoff
                cutkwargs = keywoard arguments to the optimizer
                cut_out = cutoff output which is some boolean-interpretable value
                    - cut_out = cutoff(self,optim_out,*cutargs,**cutkwargs)
                    - This should be equivalent to:
                        cut_out = True/False # whether or not the optimization should be cut short
                    Tip: cut = lambda *args,**kwargs: False # is equivalent to no cut-off

            epochs:int = number of iterations for optimization to be performed

        """

        def updatedefaults(kwargs):
            """
            Defines defaults and updates these with real inputs.

            """

            def required(string):
                raise Exception(string)

            defaults = {
                "epochs": lambda *args, **kwargs: required(
                    'Number of epochs "epochs" input required. See model.train(*) dosctring.'
                ),
                "gen": lambda *args, **kwargs: required(
                    'Data Generator "gen" input required. See model.train(*) docstring.'
                ),
                "genargs": (),
                "genkwargs": {},
                "loss": lambda model, inputs, expectations, *args, **kwargs: tf.nn.l2_loss(
                    model(inputs), expectations, *args, **kwargs
                ),
                "lossargs": (),
                "losskwargs": {},
                "optim": lambda loss, *args, **kwargs: tf.optimizers.Adam(
                    *args, **kwargs
                ),
                "optimargs": (),
                "optimkwargs": {},
                "cut": lambda *args, **kwargs: False,
                "cutargs": (),
                "cutkwargs": {},
            }
            defaults.update(kwargs)
            return [defaults[key] for key in defaults.keys()]

        (
            epochs,
            gen,
            genargs,
            genkwargs,
            loss,
            lossargs,
            losskwargs,
            optim,
            optimargs,
            optimkwargs,
            cut,
            cutargs,
            cutkwargs,
        ) = updatedefaults(kwargs)

        lossrecord_train = []
        lossrecord_valid = []
        for i in tqdm.tqdm(range(int(epochs))):
            # with tf.GradientTape() as tape:
            gen_out_train = gen(self, "train", *genargs, **genkwargs)
            loss_out_train = loss(self, gen_out_train, *lossargs, **losskwargs)
            gen_out_test = gen(self, "val", *genargs, **genkwargs)
            loss_out_test = loss(self, gen_out_test, *lossargs, **losskwargs)
            # TODO: Update back-propagation

            loss_out_train.backward()
            # TODO: Update optimizer operation
            optim = tf.optimizers.Adam()
            optim_out = optim(loss_out_train, *optimargs, **optimkwargs)

            lossrecord_train.append(loss_out_train)
            lossrecord_valid.append(loss_out_test)
            if cut(optim_out, *cutargs, **cutkwargs):
                break

        return lossrecord_train[:i], lossrecord_valid[:i]


class CNN(NN_Base):
    def __init__(self, input_shape, *layerinfo):
        """
        Defines the CNN structure

        Parameters:
            layerinfo = list of tuples containing a tf.layers function
                and its parameters in kwargs format
            input_shape = expected image shape for inputs

        Outputs:
            image = CNN-processed image
        """
        super().__init__(input_shape=input_shape, layerinfo=layerinfo)

    def forward(self, input):
        """
        Runs the image through the layer structure.

        """
        for layer, parameters in self.layerinfo:
            input = layer(input, **parameters)
        return input


class UNet(NN_Base):
    def __init__(self, input_shape, depth=3, *layerinfo):
        super().__init__(input_shape=input_shape, depth=depth, layerinfo=layerinfo)
        filterdim = (1, *tuple(2 ** (level + 5) for level in range(depth + 1)))
        self.conving = [[] for level in range(self.depth)]
        for level in range(depth):
            dims = [
                (filterdim[level], filterdim[level + 1]),
                (filterdim[level + 1], filterdim[level + 1]),
                (filterdim[level + 2], filterdim[level + 1]),
                (filterdim[level + 1], filterdim[level]),
            ]
            self.conving[level] += [
                [
                    lambda *args, **kwargs: tf.nn.relu(tf.nn.conv3d(*args, **kwargs)),
                    {
                        "filters": tf.Variable(
                            tf.initializers.GlorotNormal()(
                                (1, 3, 3, dims[n_conv][0], dims[n_conv][1])
                            )
                        ),
                        "strides": (1, 1, 1, 1, 1),
                        "padding": "SAME",
                        "data_format": "NCDHW",
                        "name": "Depth:" + str(level) + " / Conv:" + str(n_conv),
                    },
                ]
                for n_conv in range(4)
            ]
        self.conving[0][0][1]["filters"] = tf.Variable(
            tf.initializers.GlorotNormal()((1, 3, 3, filterdim[0], filterdim[1]))
        )
        self.conving[0][3][1]["filters"] = tf.Variable(
            tf.initializers.GlorotNormal()((1, 3, 3, filterdim[1], filterdim[1]))
        )
        n_convs = 2
        dims = [
            (filterdim[depth], filterdim[depth + 1]),
            (filterdim[depth + 1], filterdim[depth]),
        ]
        self.conving += [
            [
                [
                    lambda *args, **kwargs: tf.nn.relu(tf.nn.conv3d(*args, **kwargs)),
                    {
                        "filters": tf.Variable(
                            tf.initializers.GlorotNormal()(
                                (1, 3, 3, dims[n_conv][0], dims[n_conv][1])
                            )
                        ),
                        "strides": (1, 1, 1, 1, 1),
                        "padding": "SAME",
                        "data_format": "NCDHW",
                        "name": "Depth:" + str(self.depth) + " / Conv:" + str(n_conv),
                    },
                ]
                for n_conv in range(n_convs)
            ]
        ]
        self.norming = [
            [
                [
                    lambda x, **kwargs: tf.nn.batch_normalization(
                        x,
                        mean=tf.reduce_mean(x, axis=1, keepdims=True),
                        variance=tf.math.reduce_variance(x, axis=1, keepdims=True),
                        offset=0,
                        scale=1,
                        variance_epsilon=1e-8,
                        **kwargs,
                    ),
                    {},
                ]
            ]
            * 4
        ] * self.depth
        self.pooling = [
            [
                tf.nn.max_pool,
                {
                    "ksize": [1, 1, 1, 2, 2],
                    "strides": [1, 1, 1, 2, 2],
                    "padding": "VALID",
                    "data_format": "NCDHW",
                },
            ]
        ] * depth
        self.upscaling = [
            [
                tf.nn.conv3d_transpose,
                {
                    "filters": tf.Variable(
                        tf.initializers.GlorotNormal()(
                            (1, 2, 2, filterdim[level + 1], filterdim[level + 1])
                        )
                    ),
                    "strides": [1, 1, 1, 2, 2],
                    "padding": "SAME",
                    "data_format": "NCDHW",
                },
            ]
            for level in range(self.depth)
        ]
        self.encoding = [
            lambda *args, **kwargs: tf.nn.relu(tf.nn.conv3d(*args, **kwargs)),
            {
                "filters": tf.Variable(
                    tf.initializers.GlorotNormal()(
                        (1, 1, 1, filterdim[1], filterdim[0])
                    )
                ),
                "strides": (1, 1, 1, 1, 1),
                "padding": "SAME",
                "data_format": "NCDHW",
                "name": "Depth:" + str(level) + " / Conv:Out",
            },
        ]

    def forward(self, input):
        """
        Modifies the forward_pass to have skip connections for structure

        """
        super().forward(input)
        with self.device:
            depthbuffer = []
            conv = input
            # down
            for level in range(self.depth):
                conv = self.conving[level][0][0](conv, **self.conving[level][0][1])
                conv = self.norming[level][0][0](conv, **self.norming[level][0][1])
                conv = self.conving[level][1][0](conv, **self.conving[level][1][1])
                conv = self.norming[level][1][0](conv, **self.norming[level][1][1])

                depthbuffer.append(conv)

                conv = self.pooling[level][0](conv, **self.pooling[level][1])

            # bottom
            conv = self.conving[self.depth][0][0](
                conv, **self.conving[self.depth][0][1]
            )
            conv = self.norming[self.depth][0][0](
                conv, **self.norming[self.depth][0][1]
            )
            conv = self.conving[self.depth][1][0](
                conv, **self.conving[self.depth][1][1]
            )
            conv = self.norming[self.depth][1][0](
                conv, **self.norming[self.depth][1][1]
            )

            # up
            for level in reversed(range(self.depth)):
                conv = self.upscaling[level][0](
                    conv,
                    output_shape=depthbuffer[level].shape,
                    **self.upscaling[level][1],
                )

                conv = tf.concat((conv, depthbuffer[level]), 1)

                conv = self.conving[level][2][0](conv, **self.conving[level][2][1])
                conv = self.norming[level][2][0](conv, **self.norming[level][2][1])
                conv = self.conving[level][3][0](conv, **self.conving[level][3][1])
                conv = self.norming[level][3][0](conv, **self.norming[level][3][1])
            conv = self.encoding[0](conv, **self.encoding[1])
        return conv


class Generator:
    def __init__(
        self,
        model,
        filelocation,
        trainfrac,
        validfrac,
        testfrac=None,
        shuffle=True,
        batch=None,
        *args,
        **kwargs,
    ):
        # validate fractions
        if trainfrac + validfrac < 1 and testfrac is None:
            testfrac = 1 - trainfrac - validfrac
        if trainfrac + validfrac + testfrac != 1:
            raise Exception("Data fractions do not equate to 1.")

        # get 29th img in filename sequence
        allfilenames = [
            glob.glob(folder + "/*.dcm")
            for folder in glob.glob(filelocation + "/*/data/")
        ] + [
            glob.glob(folder + "/*.ima")
            for folder in glob.glob(filelocation + "/*/data/")
        ]
        middleimgs = [filenames[29] for filenames in allfilenames if filenames]

        # define number of files for each type and shuffle if flagged
        ntrainfiles, nvalidfiles, ntestfiles = [
            round(frac * len(middleimgs)) if frac else 0
            for frac in [trainfrac, validfrac, testfrac]
        ]
        if ntrainfiles + ntestfiles + nvalidfiles == len(middleimgs) + 1:
            ntrainfiles -= 1
        if shuffle:
            random.shuffle(middleimgs)

        # identify files for each section
        indices = [ntrainfiles, nvalidfiles, ntestfiles, 0]
        inds = [sum(indices[:i]) for i in range(len(indices))]
        trainfiles, validfiles, testfiles = [
            middleimgs[inds[i] : inds[i] + inds[i + 1]] for i in range(3)
        ]
        self.labeledfiles = {
            "training": trainfiles,
            "validation": validfiles,
            "testing": testfiles,
        }
        self.shuffle = shuffle
        self.batch = batch
        self.args = args
        self.kwargs = kwargs
        self.mkmask(model, *args, **kwargs)
        return None

    def __call__(self, *args, **kwargs):
        return self.getdata(*args, **kwargs)

    def mkmask(
        self,
        model,
        accel_factor=5,
        poly_order=4,
        dist_order=2,
        center_diameter=0,
        dist_scaling=None,
        n=100,
        edgelen=10,
        *args,
        **kwargs,
    ):
        mymask = np.zeros(model.input_shape[3:], dtype=np.bool)

        # make distance map
        mesh = np.meshgrid(*[range(shape) for shape in reversed(mymask.shape)])
        mesh = [
            dimmesh.astype(np.single) - (dimshape - 1) / 2
            for dimmesh, dimshape in zip(mesh, reversed(mymask.shape))
        ]
        distancemap = np.zeros_like(mesh[0])
        for i, dim in enumerate(mesh):
            if dist_scaling:
                distancemap += np.abs((dim * dist_scaling[i])) ** dist_order
            else:
                distancemap += np.abs(dim) ** dist_order
        distancemap = distancemap ** (1 / dist_order)
        distancemap /= distancemap.max()

        # enforce fraction of center sampling
        mymask += distancemap <= np.percentile(distancemap, center_diameter * 100)

        # generate remaining pmf from distance map and # of samples to acquire
        pmf = (1 - distancemap) ** poly_order
        pmf[mymask] = 0
        pdf = pmf.ravel() / pmf.sum()
        n_samples = int(round(1 / accel_factor * mymask.size - mymask.sum()))

        # perform random sampling, access peak interference in image space, then pick the best one
        # that is if there are any n_samples to take
        if n_samples > 0:
            maskbuffer = [copy.deepcopy(mymask) for _ in range(n)]
            peaks = np.empty(n)
            if "verbosity" in kwargs.keys() and kwargs["verbosity"]:
                iterator = tqdm.tqdm(range(n), "Generating mask candidates")
            else:
                iterator = range(n)
            for i in iterator:
                samples = np.random.choice(mymask.size, n_samples, replace=False, p=pdf)
                maskbuffer[i].ravel()[samples] = 1
                interference = np.fft.ifftn(np.fft.ifftshift(maskbuffer[i]))
                peaks[i] = np.abs(np.real(interference[..., edgelen:-edgelen])).max()
            mymask = maskbuffer[np.argmin(peaks)]
            if "verbosity" in kwargs.keys() and kwargs["verbosity"]:
                print(
                    "Selected mask with minimum peak interference of ("
                    + str(round(peaks.min(), 4))
                    + ") from a distribution of ("
                    + str(round(peaks.mean(), 4))
                    + "+/-"
                    + str(round(peaks.std(), 4))
                    + ")."
                )

        # import matplotlib.pyplot as plt
        # minind = np.argmin(peaks)
        # minmask = maskbuffer[minind]
        # maxind = np.argmax(peaks)
        # maxmask = maskbuffer[maxind]

        # mymask = maxmask
        # samples = np.random.choice(mymask.size, n_samples, replace=False, p=pdf)
        # maskbuffer[i].ravel()[samples] = 1
        # maxinterference = np.fft.ifftn(np.fft.ifftshift(maskbuffer[i]))

        # mymask = minmask
        # samples = np.random.choice(mymask.size, n_samples, replace=False, p=pdf)
        # maskbuffer[i].ravel()[samples] = 1
        # mininterference = np.fft.ifftn(np.fft.ifftshift(maskbuffer[i]))

        # plt.plot(mininterference[5:-5],'k-',linewidth=.5)
        # plt.plot(maxinterference[5:-5],'r-',linewidth=.5)
        # plt.show()

        import tkinter as tk
        import tkinter.filedialog

        tk.Tk().withdraw()
        filename = tk.filedialog.askopenfilename(
            filetypes=(
                (
                    "numpy array",
                    "*.npy",
                ),
            )
        )
        full = np.load(filename)
        fourier = np.empty(full.shape, dtype=np.complex128)
        for img, four in zip(full[:, 0], fourier[:, 0]):
            four[:] = np.fft.fftshift(np.fft.fftn(img))
        masked = fourier * mymask
        subsampled = np.empty_like(full)
        for img, subfour in zip(subsampled[:, 0], masked[:, 0]):
            img[:] = np.fft.ifftn(np.fft.ifftshift(subfour))
        np.save("C:/temp/subsampled", subsampled[:4])

        self.mask = mymask
        return None

    def getdata(self, model, label):
        mykey = [key for key in self.labeledfiles.keys() if label in key][0]
        if self.batch:
            indices = random.sample(range(0, len(self.labeledfiles[mykey])), self.batch)
            filebatch = [self.labeledfiles[mykey][index] for index in indices]
        else:
            filebatch = self.labeledfiles[mykey]

        batchdata_in = np.empty((len(filebatch), *model.input_shape), dtype=np.single)
        batchdata_out = np.empty_like(batchdata_in)
        for i, file in enumerate(filebatch):
            rawdata = pydicom.read_file(file)
            batchdata_in[i], batchdata_out[i] = self.preprocess_data(
                rawdata, *self.args, **self.kwargs
            )
        processed_batchdata_in = self.preprocess_batch(
            batchdata_in, *self.args, **self.kwargs
        )
        return {"in": processed_batchdata_in, "out": batchdata_out}

    def preprocess_data(self, rawdata, *args, **kwargs):
        """
        Performs the image-by-image manipulation to fit the model.
        In this case, (1) a mask is generated based off a

        Parameters:
            accel_factor
            poly_order
            dist_order
            center_size


        """

        imgdata = rawdata.pixel_array.astype(np.float64)
        imgdata -= imgdata.min()
        imgdata /= imgdata.max()
        fourier = np.fft.fftshift(np.fft.fftn(imgdata))
        channeled = np.empty((2, *fourier.shape), dtype=np.single)
        channeled[0] = np.real(fourier) * self.mask
        channeled[1] = np.imag(fourier) * self.mask

        return channeled, imgdata

    def preprocess_batch(self, batchin, *args, **kwargs):
        return batchin


def mse(model, gen_out, *args, **kwargs):
    """
    Calculates the MSE loss given the model with inputs and  inputs and expectations.

    Call syntax:
        loss = mse(model,gen_out,*args,**kwargs)

    Parameters:
        model:callable = a callable model which takes in inputs
        gen_out:dict = {"in": input_tensor,"out": output_tensor}

    Outputs:
        loss = scalar calculation of MSE


    Additional argument behavior is not currently defined.

    """
    return tf.reduce_mean((model(gen_out["in"]) - gen_out["out"]) ** 2)


def main():
    """
    Tests the CNN.

    """

    # gpu = tf.config.experimental.list_physical_devices("GPU")[0]
    # tf.config.experimental.set_memory_growth(gpu, True)
    tf.compat.v1.logging.set_verbosity(40)
    model = UNet((1, 2, 512, 512), depth=4)

    gen = Generator(
        model,
        filelocation="C:\\Users\\Charlie\\Box Sync\\ENM_531_FINAL_PROJECT\\SPGR_AF2_242_242_1500_um\\",
        trainfrac=0.8,
        validfrac=0.2,
        testfrac=0,
        shuffle=True,
        batch=2,
        accel_factor=5,
        poly_order=4,
        dist_order=1,
        center_diameter=0.0,
        n=1000,
        verbosity=True,
        # dist_scaling=[1,10]
    )
    loss = mse

    model.train(gen=gen, loss=loss, epochs=1e3)
    return None


if __name__ == "__main__":
    # test_NNBase()
    main()
