# Rapid MRI via Deep Learning
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![coverage](http://gitlab.com/caosuna/rapid-mri/-/jobs/artifacts/master/raw/.test_results/coverage.svg?job=unittest)](http://gitlab.com/caosuna/rapid-mri/-/jobs/artifacts/master/file/.test_results/coverage_html/index.html?job=unittest)

Enabling clinically viable magnetic resonance image acquisition speeds with a deep learning approach. A product of the University of Pennsylvania's [Laboratory for Structural, Physiologic and Functional Imaging](https://www.pennmedicine.org/departments-and-centers/department-of-radiology/radiology-research/labs-and-centers/quantitative/lab-for-structural-physiologic-and-functional-imaging).

![image-info](docs/images/result_example.png)
A two-dimensional image of a femoral cross-section is taken from a standard ex vivo scan, simulated as undersampled at 7x in k-space (1/7th of the samples are "measured" or kept), then recovered with artifact removal via deep convolutional neural network.
## Summary
Magnetic resonance imaging (MRI) is a radiation-free imaging modality capable of assessing bone quality in vivo in a clinical setting. In combination with MRI-derived finite element (FE) simulations of bone microstructural mechanics, MRI can better inform clinical evaluation of bone health. There are, however, two significant challenges with this analysis: (1) creation of the finite element mesh from the image, requiring significant time in segmentation of thperiosteal boundary and classification of scan-specific intensities of cortical bone and marrow; (2) acquisition time can be signficant, upwards of 16 minutes, to generate a high enough fidelity to permit further automated image processing. This project seeks to tackle the second issue by investigating the ability to recover image fidelity from a sparsely sampled k-space.

For more details and results of our application for 2D slices, see the [report](reports/Results_Report.pdf).

Details in documentation and code refactoring are a work in progress. See the [references](refs) folder for the original code.

## Technical Details
### System
- Graphics Card: Nvidia GeForce RTX 2080 Ti 
- Tensorflow GPU 1.12
- Cuda Toolkit 9.0
- cuDNN 7.1.4

### Pre-processing:
Generation of Modeled Subsampled MR Images:
- Fourier Transform
- Subsampling with Pseudo-Randomly Sampled Pre-defined PDF
- Inverse Fourier Transform

### Training Info:
- Dataset size: n = 94
- Input Image Size: 512 x 512 pixels
- Split Ratio: 80:20 (training:validation)
- 1500 epochs
- Adam Optimizer
  - Learning Rate: 2e-06
- Loss Function Form: MSE

### Architecture: U-Net CNN

![image-info](docs/images/architecture.png)

### Results
See the [report](reports/Results_Report.pdf).

## Contributors
See [Contributing.MD](Contributing.MD)
