"""
    These functions expect batch and channels first in form "NC..."
    i.e. arg[0].shape = (10,2,500,500,...)
    """
import numpy as np
import skimage
import skimage.measure


def my_metrics_2D(label, subsampled, prediction):

    img_height, img_width, num_images = label.shape

    ssim_sub = np.empty((num_images), dtype=np.double)
    nrmse_sub = np.empty((num_images), dtype=np.double)
    psnr_sub = np.empty((num_images), dtype=np.double)

    for image in range(num_images):
        ssim_sub[image] = ssim(lab=label[:, :, image], pred=subsampled[:, :, image])
        nrmse_sub[image] = nrmse(lab=label[:, :, image], pred=subsampled[:, :, image])
        psnr_sub[image] = psnr(lab=label[:, :, image], pred=subsampled[:, :, image])

    ssim_pred = np.empty((num_images), dtype=np.double)
    nrmse_pred = np.empty((num_images), dtype=np.double)
    psnr_pred = np.empty((num_images), dtype=np.double)

    for image in range(num_images):
        ssim_pred[image] = ssim(lab=label[:, :, image], pred=prediction[:, :, image])
        nrmse_pred[image] = nrmse(lab=label[:, :, image], pred=prediction[:, :, image])
        psnr_pred[image] = psnr(lab=label[:, :, image], pred=prediction[:, :, image])

    data_frame = np.zeros((num_images, 6), dtype=np.double)

    data_frame[:, 0] = ssim_sub
    data_frame[:, 1] = nrmse_sub
    data_frame[:, 2] = psnr_sub

    data_frame[:, 3] = ssim_pred
    data_frame[:, 4] = nrmse_pred
    data_frame[:, 5] = psnr_pred

    return data_frame
    # return ssim_sub, nrmse_sub, psnr_sub, ssim_pred, nrmse_pred, psnr_pred


def ssim(lab, pred):
    return skimage.measure.compare_ssim(X=lab, Y=pred)


def nrmse(lab, pred):
    return skimage.measure.compare_nrmse(im_true=lab, im_test=pred)


def psnr(lab, pred):
    return skimage.measure.compare_psnr(im_true=lab, im_test=pred)


# if __name__ == '__main__':

#
