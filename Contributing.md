
### Getting Started
Start by installing an editable version of the source and test codes into your venv. Then follow-up by installing the requirements.txt containing all libraries used for dev.
```bash
pip install -e . # includes module and test module dependencies
pip install -r requirements.txt # includes dev-only dependencies: linter, formatter, etc.
```
Requirements for pull request:
- Black format compliance
- All tests are passing
- 100% coverage of test and source code

### Black-compliance
Check compliance of code with black for Python using:
```bash
black --check src tests
```
Note: Keep in mind auto-formatting may not apply the required `\n` to the end of your file. If `black` is still denoting improper format, be sure to check this.

### Testing
Tests are generated and run via Python's built-in `unittest` library.
Tests should be run through the test module while in your venv:
```bash
python -m unittest discover tests
```
Note: Empty `__init__.py` files are needed in directories of tests to be discovered by unittest. Also suggest ignoring `__main__.py` files in coverage as these should be an extremely simple script to interface with other components of the module.

### Coverage
With a coverage configurable file (`.coveragerc`), running properly configured evaluation of coverage is easy with the Python `coverage` package.
Coverage can be assessed and summarized as simply as:
```bash
coverage run # equivalently: python -m coverage run
coverage report # outputs result to console
coverage html # to an html visualization of coverage
coverage xml # to a popular parsable file format
coverage json # to an alternative parsable file format
```

An example `.coveragerc` which can be used for any project is shown below:
```bash
[run]
branch = True
command_line =  -m unittest discover tests
data_file = .test_results/.coverage
source = src,tests
omit = *__main__.py

[report]
exclude_lines =
    pragma: no cover
fail_under = 100

[html]
directory = .test_results/coverage_html

[xml]
output = .test_results/coverage.xml

[json]
output = .test_results/coverage.json
```
