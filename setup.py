import setuptools, glob

with open("README.md", "r") as fh:
    readme = fh.read()

version = "0.0.1"
author = "Carlos Osuna"
author_email = "charlie@caosuna.com"
ldct = "text/markdown"
url = "https://gitlab.com/caosuna/rapid-mri"
dependencies = [
    "tensorflow==2.5.0",
    "numpy==1.19.5"
]

setuptools.setup(
    name="rapidmri",
    version=version,
    author=author,
    author_email=author_email,
    description="Rapid MRI",
    long_description=readme,
    long_description_content_type=ldct,
    url=url,
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=dependencies,
    python_requires=">=3.7",
    scripts=glob.glob("bin/*"),
)
