import numpy as np
import tkinter as tk
import tkinter.filedialog


def main(verbosity=True):
    """
    Example of loading NumPy array with tkinter.

    """
    tk.Tk().withdraw()
    filename = tk.filedialog.askopenfilename(
        filetypes=(
            (
                "numpy array",
                "*.npy",
            ),
        )
    )

    if verbosity:
        print("Opening: " + filename)

    ndarray = np.load(filename)

    if verbosity:
        print(ndarray)

    return ndarray


if __name__ == "__main__":
    main()
