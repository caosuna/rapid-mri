"""
    These functions expect batch and channels first in form "NC..."
    i.e. arg[0].shape = (10,2,500,500,...)
    """
import numpy as np
import skimage
import skimage.measure


def batch(func):
    def wrapper(*args, **kwargs):
        vals = np.empty(args[0].shape[:2])
        for batch in range(args[0].shape[0]):
            for channel in range(args[0].shape[1]):
                newargs = [arg[batch][channel] for arg in args]
                vals[batch, channel] = func(*newargs, **kwargs)
        return vals

    return wrapper


@batch
def ssim(*args, **kwargs):
    return skimage.measure.compare_ssim(*args, **kwargs)


@batch
def nrmse(*args, **kwargs):
    return skimage.measure.compare_nrmse(*args, **kwargs)


@batch
def psnr(*args, **kwargs):
    return skimage.measure.compare_psnr(*args, **kwargs)


# if __name__ == '__main__':

#
