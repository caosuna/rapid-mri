###############################
###############################
###############################
###############################
#########
#########
#########   CREATED BY: BRANDON CLINTON JONES
#########       APRIL 22, 2020
#########
#########
#########
###############################
###############################
###############################
###############################


import tensorflow as tf
import numpy as np
import timeit
from tensorflow.python import roll
import os
from matplotlib import pyplot as plt


#############
#############
#############       CUSTOM DEPENDENCIES!!!!!
#############
#############

import model_architectures
from data_loader_class import data_generator
from show_3d_images import show_3d_images

# from main_BCJ import CNN
from main_BCJ import CNN


if __name__ == "__main__":

    # name = "b_1_af_5_e_500_pf_4_lr_1e-05_gradient_center_difference"
    name = "b_1_af_5_e_500_pf_4_lr_2e-06_imagekspace_justdy"

    if os.path.isdir("E:\\ENM_Project\\SPGR_AF2_242_242_1500_um\\"):
        study_dir = "E:\\ENM_Project\\SPGR_AF2_242_242_1500_um\\"
        project_folder = "E:\\ENM_Project\\"
        test_dir = "E:\\ENM_Project\\Test_Data\\"
        print("WINDOWS\n\n")
    elif os.path.isdir("/run/media/bellowz"):
        study_dir = "/run/media/bellowz/S*/ENM_Project/SPGR_AF2_242_242_1500_um/"
        project_folder = "/run/media/bellowz/Seagate Backup Plus Drive/ENM_Project/"
        test_dir = "/run/media/bellowz/Seagate Backup Plus Drive/ENM_Project/Test_Data/"
        print("LINUX \n\n")

    print("CREATING DATASET \n\n\n")

    # my_gen = data_generator(
    #    create_new_dataset = True,
    # )

    my_gen = data_generator(create_new_dataset=False)

    print("\n\n\n DONE!!!!! \n\n\n")
